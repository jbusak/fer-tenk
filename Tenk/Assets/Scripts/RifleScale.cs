﻿using UnityEngine;
using UnityEngine.UI;

public class RifleScale : MonoBehaviour
{
    private float maxX = 1.0f;
    private float maxY = 1.0f;
    private float maxZ = 1.0f;

    protected GameObject slider;

    private void Start()
    {
        slider = GameObject.Find("Slider");
    }

    void Update()
    {
        float percentage = slider.GetComponent<Slider>().value;

        transform.localScale = new Vector3(maxX * percentage, maxY * percentage, maxZ * percentage);
    }
}