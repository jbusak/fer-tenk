﻿using UnityEngine;
using UnityEngine.UI;

public class TankScale : MonoBehaviour
{
    private float maxX = 0.1f;
    private float maxY = 0.1f;
    private float maxZ = 0.1f;

    protected GameObject slider;

    private void Start()
    {
        slider = GameObject.Find("Slider");
    }

    void Update()
    {
        float percentage = slider.GetComponent<Slider>().value;

        transform.localScale = new Vector3(maxX * percentage, maxY * percentage, maxZ * percentage);
    }
}