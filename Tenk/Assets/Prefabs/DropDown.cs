﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropDown : MonoBehaviour
{

    // Start is called before the first frame update
    private void OnEnable()
    {
        transform.position = new Vector3(0, 10, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void MoveCar()
    {
        transform.localPosition += new Vector3(0, 10, 0);
        transform.eulerAngles += new Vector3(5, 20, 5);
    }

}
